from flask import Flask, request, render_template
import urllib.request
import json
import os
import ssl

app = Flask(__name__)


def allowSelfSignedHttps(allowed):
    # bypass the server certificate verification on client side
    if allowed and not os.environ.get('PYTHONHTTPSVERIFY', '') and getattr(ssl, '_create_unverified_context', None):
        ssl._create_default_https_context = ssl._create_unverified_context


allowSelfSignedHttps(True)


@app.route('/')
def index():
    return render_template('page1.html')


@app.route('/predict', methods=['POST'])
def predict():
    age = request.form.get('age')
    sex = request.form.get('sex')
    restingBP = request.form.get('restingBP')

    data = {
        "Inputs": {
            "input1": [
                {
                    "Age": int(age),
                    "Sex": sex,
                    "RestingBP": int(restingBP),
                    "ChestPainType": "ASY",
                    "Cholesterol": 218,
                    "FastingBS": 0,
                    "RestingECG": "Normal",
                    "MaxHR": 108,
                    "ExerciseAngina": True,
                    "Oldpeak": 1.5,
                    "ST_Slope": "Flat"
                }
            ]
        },
        "GlobalParameters": {}
    }

    body = str.encode(json.dumps(data))

    url = 'http://9a5da49f-e77f-4aa9-a123-effc49910fc2.westeurope.azurecontainer.io/score'
    api_key = 'jHrVboewgsKDe6flrI1WKg5MeDh51tH1'
    headers = {'Content-Type': 'application/json',
               'Authorization': ('Bearer ' + api_key)}

    req = urllib.request.Request(url, body, headers)

    try:
        response = urllib.request.urlopen(req)
        result = response.read()
        return render_template('page1.html', prediction=result)
    except urllib.error.HTTPError as error:
        return f"Error: {error}", 500


if __name__ == '__main__':
    app.run(debug=True)
